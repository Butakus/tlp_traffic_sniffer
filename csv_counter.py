#!/usr/bin/python2

import util
from util import log, checkFilePath

from subprocess import Popen,PIPE
from time import time
from glob import glob
from threading import Thread
from multiprocessing import Lock
import argparse
import sys
import os
import re

dataPath = 'data/'
resultsPath = 'results/'
util.logFile = 'logs/csv_counter.log'
csvUserIPsFile = 'user-subnet_IPs_count_per_min.csv'
csvWifiIPsFile = 'wifi-subnet_IPs_count_per_min.csv'
csvTotalFile = 'total_count_per_min.csv'
csvUserIPsPath = resultsPath + csvUserIPsFile
csvWifiIPsPath = resultsPath + csvWifiIPsFile
csvTotalPath = resultsPath + csvTotalFile
csvIPsHeader = 'Date;Time;IP;Packets;Bytes\n'
csvTotalHeader = 'Date;Time;Users Packets;Users Bytes;Wifi Packets;Wifi Bytes\n'

''' Private IPs:
        Users: 217.124.208/23
        Wifi: 217.124.210/23
'''
# Patterns:
userIPPattern = re.compile('217.124.21[6-7]')
wifiIPPattern = re.compile('217.124.21[8-9]')
privateIPPattern = re.compile('217.124.21[6-7]|217.124.21[8-9]')
userBroadcastIP = '217.124.217.255'
wifiBroadcastIP = '217.124.219.255'
broadcastIP = '255.255.255.255'

counters = {
    'user' : {},
    'wifi'    : {},
    'totalUser' : [0,0],
    'totalWifi' : [0,0]
}
timers = {
    'curMin':'',
    'curTime':'',
    'curDate':'',
    'lastTime':''
}


firstRun = True


def getCSV(pcapFile):
    args = 'tshark -r {} -t ad -n'.format(pcapFile)
    output = None
    try:
        tsharkProcess = Popen(args, shell=True, stdout=PIPE)
        log('tshark process id: {}'.format(tsharkProcess.pid))
    except Exception as eProc:
        log('Exception in process with file {f}\n{e}'.format(f=pcapFile,e=eProc.message))
        tsharkProcess.terminate()
        raise eProc
    finally:
        output = tsharkProcess.communicate()[0]
    return output


def getPublicPrivate(ipSrc, ipDst):
    if ipDst == userBroadcastIP or ipDst == wifiBroadcastIP or ipDst == broadcastIP:
        return None, None

    src_priv = privateIPPattern.match(ipSrc)
    dst_priv = privateIPPattern.match(ipDst)
    if src_priv and dst_priv:
        return None, None
    if not src_priv and not dst_priv:
        return None, None
    if src_priv:
        return ipDst, ipSrc
    return ipSrc, ipDst

def isUserIP(ip):
    if userIPPattern.match(ip):
        return True
    return False

def isWifiIP(ip):
    if wifiIPPattern.match(ip):
        return True
    return False


def updateResultUser(cnt, datetime):
    date, time = datetime
    with open(csvUserIPsPath, 'a') as ipFile:
        for ip in cnt['user']:
            line = '{d};{t};{i};{c};{b}\n'.format(d=date,t=time,i=ip,
                c=cnt['user'][ip][0],b=cnt['user'][ip][1])
            ipFile.write(line)

def updateResultWifi(cnt, datetime):
    date, time = datetime
    with open(csvWifiIPsPath, 'a') as ipFile:
        for ip in cnt['wifi']:
            line = '{d};{t};{i};{c};{b}\n'.format(d=date,t=time,i=ip,
                c=cnt['wifi'][ip][0],b=cnt['wifi'][ip][1])
            ipFile.write(line)

def updateResultTotal(cnt, datetime):
    date, time = datetime
    with open(csvTotalPath, 'a') as totalFile:
        line = '{d};{t};{uc};{ul};{wc};{wl}\n'.format(d=date,t=time,uc=cnt['totalUser'][0],ul=cnt['totalUser'][1],
            wc=cnt['totalWifi'][0],wl=cnt['totalWifi'][1])
        totalFile.write(line)

def updateResult(cnt, datetime):
    # Save into file with different threads
    log ("Updating result files...")
    t_user = Thread(target=updateResultUser, args=(cnt, datetime))
    t_user.start()
    t_wifi = Thread(target=updateResultWifi, args=(cnt, datetime))
    t_wifi.start()
    t_total = Thread(target=updateResultTotal, args=(cnt, datetime))
    t_total.start()

    t_user.join()
    t_wifi.join()
    t_total.join()
    log ("Finished writing to files")


def countIps(pcapFile, csv):
    global firstRun; global timers; global counters
    log('Processing file {}'.format(pcapFile))
    index = -1
    if not firstRun:
        # Search the last duplicated packet and discard everything before this packet (included)
        for i in xrange(len(csv)-1): #Last line is always empty
            lineTime = csv[i].split()[2]
            if lineTime == timers['lastTime']:
                index = i
                break
    validData = csv[index+1:] # Remove duplicated
    for i in xrange(len(validData)-1):
        fields = validData[i].split()
        date, time = fields[1],fields[2]
        t = time.split(':')
        tmin = t[1]; time = t[0] + ":" + t[1]
        ipSrc = fields[3]
        ipDst = fields[5]
        # Tricky here. There may be more fields before the length,
        # but the length is the first field that is a number
        index = 7
        length = 1
        while True:
            try:
                length = int(fields[index])
                break
            except ValueError:
                index += 1

        if firstRun:
            timers['curMin'] = tmin; timers['curTime'] = time; timers['curDate'] = date
        elif tmin != timers['curMin']:
            # Minute change. Update the result.
            updateResult(counters, (timers['curDate'], timers['curTime']))
            # Reset counters and update timers
            counters = {'user':{},'wifi':{},'totalUser':[0,0],'totalWifi':[0,0]}
            timers['curMin'] = tmin; timers['curTime'] = time; timers['curDate'] = date
        # Check IP pair and update counters
        publicIP, privateIP = getPublicPrivate(ipSrc, ipDst)
        if publicIP is not None:
            if isUserIP(privateIP): # User Subnet
                c = counters['user'].setdefault(publicIP,[0,0])
                c[0] += 1; c[1] += int(length)
                counters['user'][publicIP] = c
                counters['totalUser'][0] += 1; counters['totalUser'][1] += int(length);
            elif isWifiIP(privateIP): # Wifi Subnet
                c = counters['wifi'].setdefault(publicIP,[0,0])
                c[0] += 1; c[1] += int(length)
                counters['wifi'][publicIP] = c
                counters['totalWifi'][0] += 1; counters['totalWifi'][1] += int(length);
        firstRun = False

    timers['lastTime'] = validData[len(validData)-2].split()[2]
    log('Finished processing file {}'.format(pcapFile))


def getVersion():
    """ Print this program version """
    v = '\nLAN Capture Analyzer Beta 0.1\n'
    print v


def getFullVersion():
    """ Print this program version and the version of tshark and pigz"""
    v = 'Tshark version info:\n'
    a = Popen('tshark -v', shell=True, stdout=PIPE)
    v += a.communicate()[0]
    print v


def updateArgs():
    """ Get the arguments from the command line and update the global variables """
    global dataPath; global resultsPath; global csvUserIPsPath; global csvWifiIPsPath; global csvTotalPath

    parser = argparse.ArgumentParser(description="""Process traffic captures and output a CSV file with the number 
                                                    of occurrences and bytes of each public IP each minute.""")
    parser.add_argument('-d', '--data-path', metavar='data_path',
        help='Directory to take the data from. Default: {}'.format(dataPath))
    parser.add_argument('-r', '--results-path', metavar='results_path',
        help='Directory to save the CSV output files. Default: {}'.format(resultsPath))
    parser.add_argument('-l', '--log', metavar='log_file',
        help='File to output the logs. Default: {}'.format(util.logFile))
    parser.add_argument('-v', '--version', action='version', version=getVersion(),
        help='Print the version and exit')
    parser.add_argument('-V', '--full-version', metavar='full_version', action='store_const', const=1,
        help='Print the version of all used programs and exit')
    args = parser.parse_args()
    
    if args.data_path:
        dataPath = args.data_path
    if args.results_path:
        resultsPath = args.results_path
        csvUserIPsPath = os.path.join(resultsPath, csvUserIPsFile)
        csvWifiIPsPath = os.path.join(resultsPath, csvWifiIPsFile)
        csvTotalPath = os.path.join(resultsPath, csvTotalFile)
    if args.log:
        util.logFile = args.log
    if args.full_version:
        getFullVersion()
        exit(0)


def checkHeader(path, header):
    try:
        f = open(path, 'rw')
        if f.readline() != header:
            print '{} file was initiailzed with a wrong header. Fix it or remove it!'.format(path)
            f.close()
            sys.exit(1)
        else:
            firstRun = False
            f.close()
    except IOError: # File not created
        f = open(path, 'w')
        f.write(header)
        f.close()
        firstRun = True

if __name__ == '__main__':
    # Get commandline arguments and update global variables
    updateArgs()

    # Check and create paths if necessary
    checkFilePath(util.logFile)
    checkFilePath(csvUserIPsPath)

    # Check file headers if they were already created
    checkHeader(csvUserIPsPath, csvIPsHeader)
    checkHeader(csvWifiIPsPath, csvIPsHeader)
    checkHeader(csvTotalPath, csvTotalHeader)

    # Thread for the counting
    counter_thread = Thread()

    # Get all capture files in the data directory
    pcapFiles = glob(dataPath + '*.pcap.gz')
    pcapFiles.sort() # Order the list. Older files first.
    for f in xrange(len(pcapFiles)):
        log('Starting file "{}"'.format(pcapFiles[f]))
        csv = ''
        try:
            csv = getCSV(pcapFiles[f])
        except Exception as eMain:
            log('Exception in main. File: {}'.format(pcapFiles[f]))
            try:
                log('Insisting on file {}'.format(pcapFiles[f]))
                csv = getCSV(pcapFiles[f])
            except Exception as eMain2:
                log('Another exception. File: {}'.format(pcapFiles[f]))
                log('Time to GTFO')
                sys.exit(1)
        finally:
            # Wait until the counter thread finishes
            if counter_thread.isAlive():
                counter_thread.join()
            # And launch the next one
            counter_thread = Thread(target=countIps, args=(pcapFiles[f], csv.split("\n")))
            counter_thread.start()
    # In the end save what's left in the counters
    if counter_thread.isAlive():
        counter_thread.join()
    updateResult(counters, (timers['curDate'], timers['curTime']))
