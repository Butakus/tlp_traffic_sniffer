from subprocess import Popen,PIPE
from time import localtime, strftime
from glob import glob
from threading import Thread
import sys

csvFileName = 'results/test2.csv'
threaded = True # Set to false if the system does not have so much RAM
threads = {}

# NOTE: Field names may differ in different versions of tshark.
fields = [
    '_ws.col.UTC',
    '_ws.col.Source',
    '_ws.col.Destination',
    #'"_ws.col.Source port"',
    #'"_ws.col.Destination port"',
    #'_ws.col.Protocol',
    '_ws.col.Length'
    ]
argFields = ''
for f in fields:
    argFields += ' -e '
    argFields += f

header = 'DateTime;Source;Destination;Source port;Destination port;Protocol\n'
firstRun = True

def getCSV(pcapFile):
    args = ('tshark -r {fi} -T fields -t u -E separator=, -E occurrence=f {argFields}'.format(fi=pcapFile, argFields=argFields, fo=(pcapFile + '.csv')))
    try:
        tsharkProcess = Popen(args, shell=True, stdout=PIPE)
        print 'tshark process id: {}'.format(tsharkProcess.pid)
    except Exception as eProc:
        print 'Exception in process with file {}'.format(pcapFile)
        print eProc.message
        tsharkProcess.terminate()
        raise eProc
    finally:
        ret = tsharkProcess.communicate()
        if ret[1]: # stderr
            print 'Error with tshark: {}'.format(ret[1])
        return ret[0]

def appendCSV(pcapFile, csv):
    global firstRun
    print 'Appending file {}'.format(pcapFile)
    index = -1
    if not firstRun:
        last = lastTime()
        # print 'Last time: {}'.format(last) #Debug
        # Search the last duplicated packet and discard everything before this packet (included)
        for i in xrange(len(csv)-1): #Last line is always empty
            lineTime = csv[i].split(",")[0]
            if lineTime == last:
                index = i
                break
    validData = csv[index+1:] # Remove duplicated
    csvFile = open(csvFileName, 'a') # Open csv file in append mode
    for i in xrange(len(validData)-1):
        csvFile.write(validData[i] + "\n")
    csvFile.close()
    firstRun = False
    if threaded:
        threads.pop(pcapFile)

def lastTime():
    csvFile = open(csvFileName, 'r')
    csvFile.seek(-2,2) # Set the cursor in the end of the file.
    while csvFile.read(1) != "\n":
        csvFile.seek(-2,1) # Move the cursor until the last line's start
    lastLine = csvFile.readline().split(',') # Get fields of last line
    return lastLine[0] # Time field in pos 0


if __name__ == '__main__':
    try:
        csvFile = open(csvFileName, 'rw')
        if csvFile.readline() != header:
            print 'CSV file was initiailzed with a wrong header. Fix it or remove it!'
            csvFile.close()
            sys.exit(1)
        else:
            firstRun = False
            csvFile.close()
    except IOError: # File not created
        csvFile = open(csvFileName, 'w')
        csvFile.write(header)
        csvFile.close()
        firstRun = True

    pcapFiles = glob('data/*.pcap.gz')
    pcapFiles.sort() # Order the list. Older files first.
    for f in xrange(len(pcapFiles)):
        print 'Starting file "{}"'.format(pcapFiles[f])
        csv = ''
        try:
            csv = getCSV(pcapFiles[f])
        except Exception as eMain:
            print 'Exception in main. File: {}'.format(pcapFiles[f])
            try:
                print 'Insisting on file {}'.format(pcapFiles[f])
                csv = getCSV(pcapFiles[f])
            except Exception as eMain2:
                print 'Another exception. File: {}'.format(pcapFiles[f])
                if threaded:
                    print 'Waiting for last appends to finish...'
                    lastThreads = threads.values()
                    for t in lastThreads:
                        print 'Finishing append thread...'
                        t.join()
                print 'Time to GTFO'
                sys.exit(1) 
        finally:
            if threaded:
                appendThread = Thread(target=appendCSV, args=(pcapFiles[f], csv.split("\n")))
                threads[pcapFiles[f]] = appendThread
                appendThread.start()
            else:
                appendCSV(pcapFiles[f], csv.split("\n"))
