#!/usr/bin/python2

import util
from util import log, checkFilePath

from subprocess import Popen, PIPE
from time import time, sleep, strftime
from threading import Thread
import argparse
import signal
import os
from sys import exit


# Default values for global variables
# Interface to capture packets
interface = 'eth0'
# File to store the logs
util.logFile = 'logs/capture.log'
# Default path to save the caprures
dataPath = 'data/'
# Default capture name
fileName = 'captura.pcap'
# Timestamp format for capture files (see python time library for more info)
timeFormat = '%Y-%m-%d_%H-%M-%S_'
# Final capture file path + name.
capFileName = os.path.join(dataPath, timeFormat + fileName)
# Time between different captures
timeInterval = 180
# tshark capture buffer (bytes). See tshark manual for -B option.
captureBuffer = 4096 #8192
# gzip compress ratio [1-9] 1 is faster.
compressRatio = 5

# Process ID of the oldest tshark process (the first to finish and clean)
oldTsharkProcessId = None

# Dictionary with all current tshark processes
# key = tshark process ID
# val = process object
runningProcesses = {}

# Dictionary with all current open pcap files (capturing and compressing)
# key = tshark process ID which generated that file
# val = filename
openFiles = {}

# Dictionary with all current gzip threads
# key = tshark process ID which generated the file to compress
# val = thread object
zipThreads = {}

# Variable to detect if this process was killed (Ctrl-C or kill)
process_killed = False


def startTshark(fileName):
    """ Start a new tshark process with the given file and return the process object """
    try:
        args = 'tshark -i {i} -a duration:{t} -B {b} -s 64 -w {f}'.format(i=interface, t=timeInterval, b=captureBuffer, f=fileName)
        tsharkProcess = Popen(args, stdout=PIPE, stderr=PIPE, shell=True)
        log('Starting capture on file: {}'.format(fileName))
        log('tshark process id: {}'.format(tsharkProcess.pid))
        return tsharkProcess
    except Exception as e:
        tsharkProcess.terminate()
        raise e


def zipWorker(processId):
    """ Start a new pigz process and wait until it finishes """
    pcapFile = openFiles[processId]
    log('Compressing file: {}...'.format(pcapFile))
    args = 'pigz -p 2 -{c} {f}'.format(c=compressRatio, f=pcapFile)
    gzipProcess = Popen(args, shell=True)
    log('pigz process id: {}'.format(gzipProcess.pid))
    gzipProcess.wait()
    # Remove items from lists when gzip finish
    openFiles.pop(processId)
    zipThreads.pop(processId)
    log('pigz finished. Process id: {}'.format(gzipProcess.pid))


def gzip(processId):
    """ Start a new thread with a pigz process """
    zipThread = Thread(target=zipWorker, args=(processId,))
    zipThreads[processId] = zipThread
    zipThread.start()

def sleepAndPoll(process):
    """ Sleep for timeInterval while checking if the current tshark process finished before it time """
    sleep_time = max(1, timeInterval - 2)
    start_time = time()
    while (time() - start_time) < sleep_time and not process.poll():
        sleep(0.1)

def updateArgs():
    """ Get the arguments from the command line and update the global variables """
    global interface; global timeInterval; global captureBuffer; global dataPath; global fileName; global compressRatio

    parser = argparse.ArgumentParser(description='Capture traffic packets with tshark and compress the .pcap file with gzip.')
    parser.add_argument('-i', '--interface', metavar='interface',
        help='Interface to capture the traffic. Default: {}'.format(interface))
    parser.add_argument('-t', '--time-interval', metavar='time_interval', type=int,
        help='Time between files. One new file will be created after this time. Default: {}'.format(timeInterval))
    parser.add_argument('-b', '--buffer', metavar='buffer', type=int,
        help='Size of the buffer to save packets before storing into disk. Default: {}'.format(captureBuffer))
    parser.add_argument('-d', '--data-path', metavar='data_path',
        help='Directory to save captured files. Default: {}'.format(dataPath))
    parser.add_argument('-f', '--file-name', metavar='file_name',
        help='Name of the output files (.pcap). Timestamp will be automatically added. Default: {}'.format(fileName))
    parser.add_argument('-l', '--log', metavar='log_file',
        help='File to output the logs. Default: {}'.format(util.logFile))
    parser.add_argument('-c', '--compress', metavar='compress', type=int, choices=range(1,10),
        help='Gzip compress ratio [1-9]. 1 is faster, 9 is better. Default: {}'.format(compressRatio))
    parser.add_argument('-v', '--version', action='version', version=getVersion(),
        help='Print the version and exit')
    parser.add_argument('-V', '--full-version', metavar='full_version', action='store_const', const=1,
        help='Print the version of all used programs and exit')
    args = parser.parse_args()
    
    if args.interface:
        interface = args.interface
    if args.time_interval:
        timeInterval = args.time_interval
    if args.buffer:
        captureBuffer = args.buffer
    if args.data_path:
        dataPath = args.data_path
    if args.file_name:
        fileName = args.file_name
        capFileName = os.path.join(dataPath, timeFormat + fileName)
    if args.log:
        util.logFile = args.log
    if args.compress:
        compressRatio = args.compress
    if args.full_version:
        getFullVersion()
        exit(0)


def getVersion():
    """ Print this program version """
    v = '\nLAN ring sniffer Beta 0.1\n'
    print v


def getFullVersion():
    """ Print this program version and the version of tshark and pigz"""
    v = 'Tshark version info:\n'
    a = Popen('tshark -v', shell=True, stdout=PIPE)
    v += a.communicate()[0]
    v += '\npigz version info:\n'
    a = Popen('pigz -V', shell=True, stdout=PIPE, stderr=PIPE)
    v += a.communicate()[1]
    print v


def kill_signal_handler(signal, frame):
    """ Callback to handle the SIGINT signal received when this process is killed """
    global process_killed
    log('Process killed')
    process_killed = True


if __name__ == '__main__':
    # Get commandline arguments and update global variables
    updateArgs()

    # Check and create paths if necessary
    checkFilePath(util.logFile)
    checkFilePath(capFileName)
    
    # Start the listener to handle the kill signal from ctrl-C
    signal.signal(signal.SIGINT, kill_signal_handler)

    innerErrors = 0
    try:
        while not process_killed:
            # Name of the next pcap file (with the formatted time)
            newFileName = strftime(capFileName)
            try:
                newTsharkProcess = startTshark(newFileName)
                runningProcesses[newTsharkProcess.pid] = newTsharkProcess
                openFiles[newTsharkProcess.pid] = newFileName               
            except Exception as e2:
                log('Exception in layer 2:')
                log(e2.message)
                if innerErrors >= 3:
                    log('Too many inner errors. Raising error...')
                    raise e2
                log('Recovering from inner error...')
                innerErrors += 1
            finally:
                if oldTsharkProcessId:
                    runningProcesses[oldTsharkProcessId].wait()
                    runningProcesses.pop(oldTsharkProcessId)
                    log('tshark finished. Process id: {}'.format(oldTsharkProcessId))
                    gzip(oldTsharkProcessId)
                sleepAndPoll(newTsharkProcess)
                oldTsharkProcessId = newTsharkProcess.pid

    except Exception as e1:
        log('Fatal exception in layer 1:')
        log(e1.message)
        raise e1
    finally:
        log('Finishing still active processes...')
        while len(runningProcesses) > 0:
            pid, tsharkProcess = runningProcesses.popitem()
            log('Finishing tshark process {}'.format(pid))
            tsharkProcess.wait()
            log('File: {}'.format(openFiles[pid]))
            gzip(pid)
        while len(zipThreads) > 0:
            ziper = zipThreads.values()[0]
            log('Finishing pigz process...')
            ziper.join()
        print '\nBye!'
