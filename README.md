# **TLP Traffic Sniffer** #

Command-line tool to capture compressed network packets over a long time period for the [Teleco LAN Party](https://www.telecolanparty.org).

### How does it work? ###

This tool will use **tshark** to capture network packets from a given interface. Since tshark's output is not compressed, capturing packets over a long time period with a high network load (like a LAN party, for example) will make the disk usage extremely high. This tool will spawn tshark processes in a ring way, and when each tshark process finishes, the output *.pcap* file will be compressed to a *.pcap.gz* using **pigz** (parallel gzip).

All these scripts are multithreaded to use more efficiently the CPU, and have mechanisms to recover from tshark errors, so the network data is not lost. Also, the capture script will log everything in a *.log* file to check the program status if needed.


### Dependencies ###

The main dependencies are **tshark** and **pigz**, which can be installed with apt-get:

```
#!bash

sudo apt-get install tshark pigz
```


After installing tshark, make sure that it is enabled to capture packets without root mode. For more info, see [this](https://wiki.wireshark.org/CaptureSetup/CapturePrivileges)

### Installation ###

The python scripts will not be installed, but executed directly.

Download the code:

```
#!bash

git clone https://bitbucket.org/Butakus/tlp_traffic_sniffer.git
```
Then cd to the repository directory and execute any of the scripts with either
```
#!bash
./script.py
```
or
```
#!bash
python script.py
```


### Usage ###
The main script to capture packets is **[capture.py](capture.py)**

To see how to use **capture.py** and check all options run:
```
#!bash
./capture.py -h
```

**capture.py** help output:
```
#!text
LAN ring sniffer Beta 0.1

usage: capture.py [-h] [-i interface] [-t time_interval] [-b buffer]
                  [-d data_path] [-f file_name] [-l log_file] [-c compress]
                  [-v] [-V]

Capture traffic packets with tshark and compress the .pcap file with gzip.

optional arguments:
  -h, --help            show this help message and exit
  -i interface, --interface interface
                        Interface to capture the traffic. Default: eth0
  -t time_interval, --time-interval time_interval
                        Time between files. One new file will be created after
                        this time. Default: 180
  -b buffer, --buffer buffer
                        Size of the buffer to save packets before storing into
                        disk. Default: 4096
  -d data_path, --data-path data_path
                        Directory to save captured files. Default: data/
  -f file_name, --file-name file_name
                        Name of the output files (.pcap). Timestamp will be
                        automatically added. Default: captura.pcap
  -l log_file, --log log_file
                        File to output the logs. Default: logs/capture.log
  -c compress, --compress compress
                        Gzip compress ratio [1-9]. 1 is faster, 9 is better.
                        Default: 5
  -v, --version         Print the version and exit
  -V, --full-version    Print the version of all used programs and exit
```

**Example 0**: Capture packets from *eth0*, recording a new file each 3 minutes (180s):
```
#!bash
./capture.py -i eth0 -t 180
```

**Example 1**: Capture packets from *eth1*, recording a new file each 2 minutes (120s), with a capture buffer of 8192 bytes. A higher buffer may avoid packets dropped:
```
#!bash
./capture.py -i eth1 -t 120 -b 8192
```

**Example 2**: Capture packets from *eth0*, recording a new file each 3 minutes (180s), and save the packets in the directory */data/capturasTLP9*:
```
#!bash
./capture.py -i eth0 -t 180 -d /data/capturasTLP9
```

---

The capture processing script to generate a CSV file with the analysis of the data is **[csv_counter.py](csv_counter.py)**. It will count the number of occurrences of each public IP address each minute. The output generated will have the number of packets and the number of bytes per public IP per minute. This data will be split into User subnet and Wifi subnet, together with and extra file with the total number of packets and bytes per minute.

To see how to use **csv_counter.py** and check all options run:
```
#!bash
./csv_counter.py -h
```

**csv_counter.py** help output:
```
#!text
LAN Capture Analyzer Beta 0.1

usage: csv_counter.py [-h] [-d data_path] [-r results_path] [-l log_file] [-v]
                      [-V]

Process traffic captures and output a CSV file with the number of occurrences
and bytes of each public IP each minute.

optional arguments:
  -h, --help            show this help message and exit
  -d data_path, --data-path data_path
                        Directory to take the data from. Default: data/
  -r results_path, --results-path results_path
                        Directory to save the CSV output files. Default:
                        results/
  -l log_file, --log log_file
                        File to output the logs. Default: logs/csv_counter.log
  -v, --version         Print the version and exit
  -V, --full-version    Print the version of all used programs and exit
```

**Example**: Take data captures (*.pcap.gz* format) from *data/* and place the CSV files in *results*. These are the default options:
```
#!bash
./csv_counter.py -d data -r results
```


---

Another capture processing script to generate a CSV file with the raw data is **[csv_full_output.py](csv_full_output.py)**. 

*This section will be updated soon*.


### TODOs ###
* Adapt and document the CSV generators to be useful and easy to use.
* Implement a CSV parser to make data analysis.
* Make an install script, so it can be used from anywhere in the system.

### Notes ###
**About privacity: ** The tshark command used only captures the first 64 bytes of each packet. This is because we only want the packet header to be analyzed. By doing this we use much less disk space, while we can guarantee the users privacity.

Although the capture script will check for errors and restart tshark, it is possible to have errors with the network card that can't be recovered with software, or maybe the command arguments were not correct. This script can stay many hours working without problem, but PLEASE check the log status periodically to make sure it is working.

### Contact ###
If you find any problem, or you want to suggest a new feature, please open a new [issue](https://bitbucket.org/Butakus/tlp_traffic_sniffer/issues) or email me at butakus[at]gmail.com.