from multiprocessing import Lock
from time import strftime
import os

# File to store the logs
logFile = 'logs/capture.log'

log_mutex = Lock()

def log(logData):
    """ Util function to log. Print in console and save into file """
    line = strftime('[%d-%m-%Y_%H:%M:%S]\t{}\n'.format(logData))
    with log_mutex:
        print line.rstrip()
        with open(logFile,'a') as f:
            f.write(line)


def checkFilePath(filePath):
    """ Creates all missing directories in the given file path """
    if not os.path.exists(os.path.dirname(filePath)):
        try:
            os.makedirs(os.path.dirname(filePath))
            log("Creating missing directory: {}".format(os.path.dirname(filePath)))
        except OSError as exc: # Guard against race condition
            if exc.errno != errno.EEXIST:
                log("Error creating directory:")
                log(exc.message)
                raise